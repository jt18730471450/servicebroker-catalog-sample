
## servicebroker-catalog-sample和版本

本程序为获取服务列表测试程序。
版本为v1。


### 需要的环境变量

服务监听端口:
* PORT (default 7000)

配置文件全路径+文件名:
* CATALOGFILE (default catalog.json)

### API接口

#### GET /v2/catalog
获取服务列表。

curl样例：
```
curl -i -X GET http://127.0.0.1:7000/v2/catalog
```
