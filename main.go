package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/pivotal-golang/lager"
)

//定义日志以及其他变量
var logger lager.Logger
var v2Catalog string = "v2catalog"
var port string
var catalogfile string

func init() {
	port = getenv("PORT")
	catalogfile = getenv("CATALOGFILE")
	if len(port) == 0 {
		port = "7000"
	}
	if len(catalogfile) == 0 {
		catalogfile = "catalog.json"
	}
	println("serve on port ", port, " and file ", catalogfile)
}

func main() {
	//初始化日志对象，日志输出到stdout
	logger = lager.NewLogger(v2Catalog)
	logger.RegisterSink(lager.NewWriterSink(os.Stdout, lager.INFO)) //默认日志级别
	router := handle()

	s := &http.Server{
		Addr:           ":" + port,
		Handler:        router,
		ReadTimeout:    30 * time.Second,
		WriteTimeout:   30 * time.Second,
		MaxHeaderBytes: 0,
	}
	logger.Info("Service starting ...", map[string]interface{}{"user": "master", "time": getTimeNow()})
	//监听端口
	s.ListenAndServe()
}

func handle() (router *gin.Engine) {
	//设置全局环境：1.开发环境（gin.DebugMode） 2.线上环境（gin.ReleaseMode）
	gin.SetMode(gin.ReleaseMode)

	router = gin.Default()

	router.GET("/v2/catalog", v2catalog)

	return
}

func v2catalog(c *gin.Context) {

	data, err := ioutil.ReadFile(catalogfile)
	if err != nil {
		logger.Error("Open "+catalogfile+" false!", err)
		c.Data(404, "application/json", data)
		return
	}
	var jsonobj interface{} = nil
	err = json.Unmarshal(data, &jsonobj)
	if err != nil {
		logger.Error("unmarshal json", err)
		c.Data(500, "application/json", []byte(err.Error()))
		return
	}
	resp, _ := json.MarshalIndent(jsonobj, "", "  ")
	logger.Info("v2/catalog", map[string]interface{}{"user": "master", "time": getTimeNow(), "result": 200})
	c.Data(200, "application/json", resp)
}

func getenv2(env string) string {
	env_value := os.Getenv(env)
	if env_value == "" {
		fmt.Println("FATAL: NEED ENV", env)
		fmt.Println("Exit...........")
		os.Exit(2)
	}
	fmt.Println("ENV:", env, env_value)
	return env_value
}

func getenv(env string) string {
	return os.Getenv(env)
}

func getTimeNow() string {
	return time.Now().Format("2006-01-02 15:04:05.00")
}
